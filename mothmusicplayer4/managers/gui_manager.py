#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
import logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

from mothmusicplayer4.components import commandline_component
from mothmusicplayer4.components import filebrowser_component
from mothmusicplayer4.components import infobar_component
from mothmusicplayer4.components import playercontrol_component
from mothmusicplayer4.components import playlist_component 
from mothmusicplayer4.components import seekbar_component
from mothmusicplayer4.components import settings_component
from mothmusicplayer4.components import style_component
from mothmusicplayer4 import singleton

class gui_manager(metaclass=singleton.singleton):

    def __init__(self, core):
        self.core = core
        self.seekbar_component = seekbar_component.seekbar_component(self)
        self.commandline_component = commandline_component.commandline_component(self)
        self.filebrowser_component = filebrowser_component.filebrowser_component(self)
        self.infobar_component = infobar_component.infobar_component(self)
        self.playercontrol_component = playercontrol_component.playercontrol_component(self)
        self.playlist_component = playlist_component.playlist_component(self)
        self.settings_component = settings_component.settings_component(self)
        self.style_components = style_component.style_component(self)

    def init(self):
        logger.info("starting gui...")
        self.get_default_layout()

    def run(self):
        Gtk.main()


    def destroy(self, widget, data = None, data2 = None):
        logger.info("destroying gui...")
        em = self.core.event_manager
        em.emit("application-destroy")
        Gtk.main_quit()

    def key_press(self, widget, event, data = None):
        keyname = Gdk.keyval_name(event.keyval)
        if keyname == "colon":
            self.commandline_component.textbox.grab_focus()
            #self.commandline_component.box.show()
            self.commandline_component.show_hide(True)

    def get_default_layout(self):
        logger.info("getting gui...")
        self.window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        self.window.set_title("Moth Music Player")
        self.window.set_name("main")
        self.window.set_default_size(1024, 512)
        #self.window.set_icon_from_file()
        self.window.connect("destroy", self.destroy)
        self.window.connect("key_press_event", self.key_press)

        #self.theme_setup()

        self.box1 = Gtk.VBox(False, 0)
        self.box2 = Gtk.HPaned()
        self.box2.set_position(self.window.get_size()[0] / 2)
        self.box3 = Gtk.HBox(False, 10)
        self.box4 = Gtk.VBox(False, 0)

        self.player_controls = self.playercontrol_component.get_gui_element()
        self.playlist = self.playlist_component.get_gui_element()
        self.file_browser = self.filebrowser_component.get_gui_element()

    
        self.command_line = self.commandline_component.get_gui_element()

        self.infobar = self.infobar_component.get_gui_element()

        self.box1.pack_start(self.player_controls, False, False, 0)
        self.box1.pack_start(self.infobar, False, False, 0)
        self.box3.pack_start(self.box2, True, True, 5)
        self.box1.pack_start(self.box3, True, True, 5)
        self.box2.add1(self.file_browser)
        self.box1.pack_start(self.command_line, False, False, 0)
        self.box2.add2(self.playlist)

        self.window.add(self.box1)

        self.box1.show()
        self.box2.show()
        self.box3.show()
        self.box4.show()
        self.window.show()
        
        #show hide filechooser
        #playlist grab focus





        

        
