#!usr/bin/env python

import threading 
import time
import socket
import logging
import sys
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

class remote:
    def __init__(self):
        self.core = None
        self.HOST = ""
        self.PORT = 8083
        self.s = None
        self.connection = None
        self.exit = False


    def destroy(self, data):
        print("remote exit")
        sys.stdout.flush()
        if self.connection is not None:
            try:
                self.connection.shutdown(socket.SHUT_RDWR)
            except:
                pass
        if self.s is not None:
            try:
                self.s.shutdown(socket.SHUT_RDWR)
            except:
                pass

    def init(self, core):
        logger.info("initializing " + __name__ + " plugin...")
        self.core = core
        em = self.core.event_manager
        em.register("application-destroy", self.destroy)

    def run(self):
        logger.info("running " + __name__ + " plugin...")
        t = threading.Thread(target = self.run_server)
        t.start()

    def run_server(self):
        print("in server...")
        sys.stdout.flush()
        self.s = None
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind((self.HOST, self.PORT))
        self.s.listen(1)
        self.connection, addr = self.s.accept()
        while True:
            data = self.connection.recv(1024)
            if data:
                print(repr(data))
                sys.stdout.flush() 
                if data.decode("utf-8") == 'play':
                    self.play_callback()
                elif data.decode("utf-8") == 'pause':
                    self.pause_callback()
                elif data.decode("utf-8") == 'next':
                    self.next_callback()
                elif data.decode("utf-8") == 'prev':
                    self.prev_callback()
                elif data.decode("utf-8") == 'exit':
                    self.connection.close()
                    self.s.close()
                    self.s = None
                    print("exit recieved")
                    sys.stdout.flush()
                    break
                self.connection.sendall(data)
            else:
                self.connection.close()
                break

    def play_callback(self):
        self.core.gui_manager.playercontrol_component.play_button_handler()

    def pause_callback(self):
        self.core.player_manager.pause_track()

    def next_callback(self):
        self.core.player_manager.start_next_track_from_playlist()

    def prev_callback(self):
        self.core.player_manager.start_previous_track_from_playlist()
