#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
from mothmusicplayer4.managers import configuration_manager
from mothmusicplayer4.managers import keybind_manager
from mothmusicplayer4.managers import gui_manager
from mothmusicplayer4.managers import library_manager
from mothmusicplayer4.managers import mediainformation_manager
from mothmusicplayer4.managers import player_manager
from mothmusicplayer4.managers import plugin_manager
from mothmusicplayer4.managers import event_manager
from mothmusicplayer4.managers import timer_manager
from mothmusicplayer4.managers import setup_manager


class core:

    def __init__(self):
        #define managers
        self.configuration_manager = configuration_manager.configuration_manager(self)
        self.library_manager = library_manager.library_manager(self)
        self.keybind_manager = keybind_manager.keybind_manager(self)
        self.event_manager = event_manager.event_manager(self)
        self.mediainformation_manager = mediainformation_manager.mediainformation_manager(self)
        self.plugin_manager = plugin_manager.plugin_manager(self)
        self.gui_manager = gui_manager.gui_manager(self)
        self.timer_manager = timer_manager.timer_manager(self)
        self.player_manager = player_manager.player_manager(self)
        self.setup_manager = setup_manager.setup_manager(self)

    def run(self):
        self.library_manager.init()
        self.gui_manager.init()
        self.plugin_manager.run()
        self.player_manager.run()
        self.setup_manager.run()
        self.gui_manager.run()
