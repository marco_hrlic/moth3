#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
import threading
import logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

class playlist_component():

    def __init__(self, manager):
        self.manager = manager
        self.store = self.create_model()
        self.prev_index = 0
        em = self.manager.core.event_manager
        em.register("library-updated", self.update_model)
        em.register("player-index-changed", self.selection_change)
        em.register("player-start-track", self.selection_change)
        em.register("player-index-changed", self.color_update)
        em.register("player-start-track", self.color_update)

    def selection_change(self, index):
        logger.info("changing selection...")
        path = Gtk.TreePath(index)
        self.treeView.set_cursor(path)

    def select_all(self):
        self.treeView.get_selection().select_all()

    def grab_focus(self):
        self.treeView.grab_focus()

    def update_model(self, data):
        logger.info("updating model...")
        if data is not None:
            items = self.manager.core.library_manager.get_items_by_list_of_hashes(data)
            if items is None:
                logger.error("cannot load from library...")
                return
            item_list = [(0, item[1],item[2], item[3], item[0], item[4]) for item in items]
            index = len(self.store) 
            for item_data in item_list:
                self.store.append([item_data[0], item_data[1], item_data[2], item_data[3], "#808080", item_data[4], "", item_data[5]])
            self.update_index()
            em = self.manager.core.event_manager
            em.emit("playlist-updated", index)


    def update_index(self):
        logger.info("updating index...")
        counter = 0;
        for item in self.store:
            item[0] = counter
            counter = counter + 1

    def create_model(self):
        logger.info("creating model...")
        items = self.manager.core.library_manager.get_items_from_library()
        data = [(int(x), item[1],item[2], item[3], item[0], item[4]) for x, item, in enumerate(items)]
        store = Gtk.ListStore(int, str, str, str, str, str, str, int)
        for item in data:
            store.append([item[0], item[1], item[2], item[3], "#808080", item[4], "", item[5]])
            #0-index, 1-title, 2-artistm 3-album, 4-color, 5-path, 6-icon, 7-hash
        return store
    
    def search_func(self, model, column, key, iterator, data=None):
        value0 = model.get_value(iterator, 0)
        value1 = model.get_value(iterator, 1)
        value2 = model.get_value(iterator, 2)
        value = str(value0) + value1 + value2
        if str(key).lower() in str(value).lower():
            return False
        return True


    def create_columns(self, treeView):
        logger.info("creating colums...")
        rendererText = Gtk.CellRendererText()
        rendererPixbuf = Gtk.CellRendererPixbuf()
        column = Gtk.TreeViewColumn("Index")
        column.pack_start(rendererText, True)
        column.set_attributes(rendererText, text=0, foreground=4)
        column.pack_start(rendererPixbuf, True)
        column.add_attribute(rendererPixbuf, "stock-id", 6)
        column.set_sort_column_id(0)
        column.set_resizable(False)
        treeView.append_column(column)

        rendererText = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Title", rendererText, text=1, foreground=4)
        column.set_sort_column_id(1)
        column.set_resizable(True)
        column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        column.set_expand(True)
        treeView.append_column(column)

        rendererText = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Artist", rendererText, text=2, foreground=4)
        column.set_sort_column_id(2)
        column.set_resizable(True)
        column.set_expand(True)
        treeView.append_column(column)

        rendererText = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Album", rendererText, text=3, foreground=4)
        column.set_sort_column_id(3)
        column.set_resizable(True)
        column.set_expand(True)
        treeView.append_column(column)


    def keypress(self, widget, event):
        keyname = Gdk.keyval_name(event.keyval)
        if keyname ==  "BackSpace":
            self.remove_selected(None, widget)
    
    def remove_selected(self, menu = None, treeView = None):
        logger.info("removing selected items...")
        if len(self.store) == 0:
            return
        selection = self.treeView.get_selection()
        paths = selection.get_selected_rows()[1]
        hashes = []
        for path in reversed(paths):
            index = path[0]
            hash_ = self.store[index][7]
            treeiter = self.store.get_iter(path)
            self.store.remove(treeiter)
            hashes.append(hash_)
            
            if index > 0:
                self.treeView.set_cursor(index)

            if index < self.prev_index:
                self.prev_index -= 1
           
        print(hashes)
        self.manager.core.library_manager.delete_items_by_list_of_hashes(hashes)
        self.update_index() 

    def row_activated(self, treeview, path, view_column, store):
        pm = self.manager.core.player_manager
        treeiter = store.get_iter(path)
        value = store.get_value(treeiter, 0)
        if pm.current_track_index < 0:
            pm.current_track_index = 0
        if pm.is_playing():
            pm.stop_track()
        print(value)
        pm.load_track_from_playlist(value)
        pm.start_track()
        self.color_update(path[0])
    
    def color_update(self, index):
        self.store[self.prev_index][4] = "#000000"
        self.store[self.prev_index][6] = ""
        #this line causes segfault 179
        #self.store[index][4] = "#2A62C9"
        self.store[index][6] = Gtk.STOCK_MEDIA_PLAY
        self.prev_index = index
        #queue

    def get_selection(self):
        return self.treeView.get_selection()

    def get_iter(self, data):
        return self.store.get_iter(data)

    def get_path(self, iter_):
        return self.store.get_path(iter_)

    def get_gui_element(self):
        logger.info("getting component gui...")
        box = Gtk.VBox(False)
        scroll_window = Gtk.ScrolledWindow()
        scroll_window.grab_focus()
        scroll_window.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
        scroll_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        box.pack_start(scroll_window, True, True, 0)

        #scroll_window.drag_dest_set(0, [], 0)
        #scroll_window.connect("drag_drop", self.drop)
        #scroll_window.connect("drag_motion", self.motion)
        self.treeView = Gtk.TreeView(self.store)
        self.treeView.set_rules_hint(True)
        self.treeView.set_enable_search(True)
        self.treeView.set_search_equal_func(self.search_func, None)
        self.treeView.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)

        self.treeView.connect("row-activated", self.row_activated, self.store)
        #self.treeView.connect("button-press-event", self.right_click)
        self.treeView.connect("key-press-event", self.keypress)

        scroll_window.add(self.treeView)
        self.create_columns(self.treeView)

        scroll_window.show()
        self.treeView.show()
        box.show()

        return box


