#!usr/bin/env python

import gi
from gi.repository import GObject
from mothmusicplayer4 import singleton
import threading

class timer_manager(metaclass=singleton.singleton):

    def __init__(self, core):
        self.core = core
        self.timer = None

    def start(self):
        sc = self.core.gui_manager.playercontrol_component.seekbar_component
        self.remove()
        self.timer = GObject.timeout_add(100, sc.update_time_label)

    def remove(self):
        if self.timer is not None:
            GObject.source_remove(self.timer)
            self.timer = None


