#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import os
import threading
import logging
import queue
import gi
import logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

from gi.repository import Gst, GObject
from mothmusicplayer4 import singleton
from mothmusicplayer4.components import timeconverter_component

class player_manager(metaclass=singleton.singleton):

    def run(self):
        logger.info("player running...")
        GObject.threads_init()
        Gst.init(None)
        #self.loop.run()
    
    def destroy(self, data, data2 = None):
        logger.info("destroying player...")
        self.stop_track()
        self.loop.quit()

    def __init__(self, core):
        self.core = core
        self.loop = GObject.MainLoop()
        self.player = None

        threading.Thread.__init__(self)
        Gst.init_check(None)
        self.ISGST010 = Gst.version()[0] == 0

        #off, playing, paused, stopped, idle
        self.player_state = "off"
        self.time_format = Gst.Format(Gst.Format.TIME)
        self.current_track_index = -2
        self.request_index_change = False

        em = self.core.event_manager
        em.register("application-destroy", self.destroy)

        self.track_queue = queue.Queue()
        self.init_player()

    def init_player(self):
        logger.info("initialazing player...")
        self.player = None
        self.player = Gst.ElementFactory.make("playbin", "player")
        self.equalizer = Gst.ElementFactory.make("equalizer-10bands", "equalizer-10bands")
        fakesink = Gst.ElementFactory.make("fakesink", "fakesink")
        self.player.set_property("video-sink", fakesink)
        audioconvert = Gst.ElementFactory.make("audioconvert", "audioconvert")
        audiosink = Gst.ElementFactory.make("autoaudiosink", "audiosink")
        sinkbin = Gst.Bin()
        sinkbin.add(self.equalizer)
        sinkbin.add(audioconvert)
        sinkbin.add(audiosink)
        self.equalizer.link(audioconvert)
        audioconvert.link(audiosink)
        sinkpad = self.equalizer.get_static_pad("sink")
        sinkbin.add_pad(Gst.GhostPad.new("sink", sinkpad))
        self.player.set_property("audio-sink", sinkbin)
        bus = self.player.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message)
        self.player.connect("about-to-finish", self.about_to_finish)
        self.player_state = "idle"

    def on_message(self, bus, message):
        t = message.type
        
        if t == Gst.MessageType.EOS:
            logger.info("player finished...")
        elif t == Gst.MessageType.ERROR:
            self.player.set_state(Gst.State.NULL)
            err, debug = message.parse_error()
            logger.error("player error:" + str(err))
            self.player_state = "idle"

    def about_to_finish(self, player):
        logger.info("player about to finish...")
        if self.core.configuration_manager.get_conf("player", "repeat"):
            #load
            self.load_track_from_playlist(self.current_track_index)
            return
        if not self.track_queue.empty():
            pass
        else:
            self.current_track_index += 1
            self.request_index_change = True
        self.load_track_from_playlist(self.current_track_index)

    def is_playing(self):
        if self.player_state == "playing":
            return True
        return False

    def is_paused(self):
        if self.player_state == "paused":
            return True
        return False

    def enqueue(self, index):
        self.track_queue.put(index)

    def empty_queue(self):
        self.track_queue.queue.clear()

    def set_track(self, path):
        logger.info("player setting track...")
        self.player.set_property("uri", path)

    def load_track(self, path):
        logger.info("player loading track...")
        if os.path.isfile(path):
            self.set_track("file://" + path)
        else:
            #emit some signal
            logger.warning("trying to load a path that is not a file...")
            pass
    
    def load_track_from_playlist(self, index):
        logger.info("player loading track from playlist...")
        lib_items = self.core.library_manager.get_items_from_library()
        items = [item[0] for item in lib_items]
        if index >= 0:
            if index < len(items):
                item_to_play = items[index]
                self.current_track_index = index
            else:
                if bool(self.core.configuration_manager.get_conf("player", "repeat_all")):
                    item_to_play = items[0]
                    self.current_track_index = 0
                else:
                    logger.warning("can not load track from playlist due to invalid index...")
                    return False
        else:
            logger.warning("can not load track from playlist due to invalid index...")
            return False
        self.load_track(item_to_play)
        em = self.core.event_manager
        em.emit("player-loading-track-finished")
        em.emit("player-index-changed", self.current_track_index)
        #emit signal
        return True

    def start_track(self, data = None):
        logger.info("player starting track...")
        self.player.set_state(Gst.State.PLAYING)
        self.player_state = "playing"
        tm = self.core.timer_manager
        tm.start()
        #emit signal
        em = self.core.event_manager
        em.emit("player-start-track", data = self.current_track_index)

    def start_next_track_from_playlist(self, data = None):
        logger.info("starting next track from playlist...")
        if self.is_playing() or self.is_paused():
            self.player.set_state(Gst.State.NULL)
            self.player_state = "idle"
            #timer
        self.current_track_index += 1
        if self.load_track_from_playlist(self.current_track_index):
            self.player.set_state(Gst.State.PLAYING)
            self.player_state = "playing"
            #timer
        else:
            self.current_track_index -= 1

    def start_previous_track_from_playlist(self, data = None):
        logger.info("starting previous track from playlist...")
        if self.is_playing() or self.is_paused():
            self.player.set_state(Gst.State.NULL)
            self.player_state = "idle"
            #timer
        self.current_track_index -= 1
        if self.load_track_from_playlist(self.current_track_index):
            self.player.set_state(Gst.State.PLAYING)
            self.player_state = "playing"
            #timer
        else:
            self.current_track_index += 1

    def stop_track(self, data = None):
        logger.info("player stoping track...")
        self.player.set_state(Gst.State.NULL)
        self.player_state = "idle"

    def pause_track(self, data = None):
        logger.info("player pause track...")
        self.player.set_state(Gst.State.PAUSED)
        self.player_state = "paused"
        #emit signal

    def get_volume(self):
        logger.info("player getting volume...")
        return float(self.player.get_property("volume")) * 100.0

    def set_volume(self, value):
        logger.info("player setting volume...")
        if not (value < 0.0 and value > 100.0):
            self.player.set_property("volume", value / 100)
            #emit signal

    def volume_track_up(self, data = None):
        logger.info("player volume up...")
        value = self.get_volume()
        if not value >= 100:
            value += 10
            self.set_volume(value)

    def volume_track_down(self, data):
        logger.info("player volume down...")
        value = self.get_value()
        if not value < 10:
            value -= 10
            self.set_volume(value)

    def get_current_position(self):
        if self.is_playing() or self.is_paused():
            current_position = self.player.query_position(self.time_format)[1]
            return timeconverter_component.convert_time(current_position)
        else:
            return None

    def get_current_position_unformatted(self):
        if self.is_playing() or self.is_paused():
            current_position = self.player.query_position(self.time_format)[1]
            return current_position
        else:
            return None

    def set_position(self, value):
        self.player.seek_simple(self.time_format, Gst.SeekFlags.FLUSH, value)

    def get_duration(self):
        return self.player.query_duration(self.time_format)[1]



