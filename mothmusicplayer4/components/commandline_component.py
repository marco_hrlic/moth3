#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import time
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

class commandline_component():

    def __init__(self, manager):
        self.manager = manager
        self.show_flag = bool(self.manager.core.configuration_manager.get_conf("player", "show_console", "bool"))
        self.is_visible = False
        self.comm_flag = False
        self.commands = []
        self.command_callback_dict = {}
        self.commands_dict = {}
        self.command_callback_data = {}

        self.setup_default_commands()


    def setup_default_commands(self):
        self.add_command("play", self.play_callback)
        self.add_command("pause", self.pause_callback)
        self.add_command("next", self.next_callback)
        self.add_command("prev", self.prev_callback)
        self.add_command("filebrowser", self.filebrowser_callback)
        self.add_command("delete_all", self.remove_all_callback)
        self.add_command("add_alias", self.add_command_alias)
        self.add_command_alias("play", "p")
        self.add_command_alias("pause", "pa")
        self.add_command_alias("next", "ne")
        self.add_command_alias("prev", "pr")
        self.add_command_alias("filebrowser", "e")
        self.add_command_alias("delete_all", "da")
        self.add_command_alias("add_alias", "map")


    def show_hide(self, flag = False):
        self.show_flag = bool(self.manager.core.configuration_manager.get_conf("player", "show_console", "bool"))
        if self.show_flag and flag:
            self.box.show()
            self.is_visible = True
        else:
            self.box.hide()
            self.is_visible = False
        self.textbox.set_text("")


    def get_tokens(self, text):
        tokens = text.split(" ")
        return tokens


    def textbox_callback(self, widget, entry):
        cm = self.manager.core.configuration_manager
        if bool(cm.get_conf("player", "show_console", "bool")):
            if self.comm_flag:
                self.show_hide()
                self.comm_flag = False
                return
            text = widget.get_text()
            result = self.evaluate(text)
            if result is not None:
                self.output(result)
            else:
                self.show_hide()


    def evaluate(self, text):
        tokens = self.get_tokens(text[1:])
        command = self.parse(tokens[0])
        result = "Unknown command."
        if command != "":
            if self.command_callback_data[command] is not None:
                result = self.command_callback_dict[command](tokens, self.command_callback_data[command])
            else:
                result = self.command_callback_dict[command](tokens)
        return result

    def add_command(self, command, callback, data = None):
        self.commands.append(command)
        self.command_callback_dict[command] = callback
        self.commands_dict[command] = [command]
        self.command_callback_data[command] = data

    def remove_command(self, command):
        del self.command_callback_dict[command]
        self.commands.remove(command)
        del self.command_callback_data[command]
        
    def add_command_alias(self, _key, value):
        try:
            key = self.parse(_key)
            if key != "":
                alias_list = self.commands_dict[key]
                alias_list.append(value)
                self.commands_dict[key] = alias_list 
                return None
            else:
                return "No " + _key + " command"
        except:
            pass

    def remove_command_alias(self, key, value):
        try:
            key = self.parse(key)
            if key != "":
                alias_list = self.commands_dict[key]
                alias_list.remove(value)
                self.commands_dict[key] = alias_list 
                return None
            else:
                return "No " + _key + " command"
        except:
            pass

    def parse(self, alias):
        for key, value in self.commands_dict.items():
            if alias in value:
                return key
        if alias in self.commands:
            return alias
        return ""
        

    def output(self, line):
        self.textbox.set_text("")
        self.textbox.set_text(line)
        self.textbox.set_position(len(line))
        self.comm_flag = True

    def key_press(self, widget, event):
        keyname = Gdk.keyval_name(event.keyval)
        if keyname == "Escape":
            self.show_hide()
            self.comm_flag = False

    def focus_out(self, widget, event):
        self.comm_flag = False
        self.show_hide()

    def get_gui_element(self):
        self.box = Gtk.HBox(True, 10)
        self.textbox = Gtk.Entry()
        self.box.pack_start(self.textbox, True, True, 0)
        self.textbox.connect("activate", self.textbox_callback, self.textbox)
        self.textbox.connect("focus_out_event", self.focus_out)
        self.textbox.connect("key_press_event", self.key_press)
        self.textbox.show()
        #self.show_hide()
        #self.box.show()
        return self.box

    def play_callback(self, tokens):
        core = self.manager.core
        pcc = core.gui_manager.playercontrol_component
        pc = core.gui_manager.playlist_component
        if len(tokens) == 1:
            pcc.play_button_handler()
        if len(tokens) > 1 and tokens[1].isdigit():
            pc.selection_change(int(tokens[1]))
            pcc.play_button_handler(True)
        return None

    def pause_callback(self, tokens):
        pm = self.manager.core.player_manager
        pm.pause_track()
        return None

    def prev_callback(self, tokens):
        pm = self.manager.core.player_manager
        pm.start_previous_track_from_playlist
        return None

    def next_callback(self, tokens):
        pm = self.manager.core.player_manager
        pm.start_next_track_from_playlist
        return None

    def filebrowser_callback(self, tokens):
        core = self.manager.core
        cm = core.configuration_manager
        state = cm.get_conf("player", "show_explorer", "bool")
        cm.set_conf("player", "show_explorer", not state)
        core.gui_manager.filebrowser_component.show_hide(not state)
        return None

    def remove_selected_callback(self, tokens):
        pc = self.manager.core.gui_manager.playlist_component
        if len(tokens) == 1:
            pc.remove_selected()
        return None

    def remove_all_callback(self, tokens):
        pc = self.manager.core.gui_manager.playlist_component
        if len(tokens) == 1:
            pc.select_all()
            pc.remove_selected()
        return None

    def add_alias_callback(self, tokens):
        result = "Not enough arguments"
        if len(tokens) == 3:
            result = self.add_commmand_alias(tokens[1], tokens[2])
        return result

    def remove_alias_callback(self, tokens):
        result = "Not enough arguments"
        if len(tokens) == 3:
            result = self.remove_commmand_alias(tokens[1], tokens[2])
        return result
