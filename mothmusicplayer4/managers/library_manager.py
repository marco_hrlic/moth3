#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from mothmusicplayer4 import singleton

import threading
import shelve
import copy
import logging
import sys
import time
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

class library_manager(metaclass=singleton.singleton):

    def __init__(self, core):
        self.core = core
        self.database_file_name = "library1"
        self.hash_ = 0

        self.cache = []
        self.cache_up_to_date = False      
        self.key_cache_up_to_date = False
        self.keys_cache = []
        self.cache_is_updating = False

        self.update_cache_in_background()

    def init(self):
        self.update_hash()

    def update_hash(self):
        try:
            logger.info("updating hash...")
            db = shelve.open(self.database_file_name)
            items = self.get_items_from_library()
            if items is None or len(items) == 0:
                return
            self.hash_ = items[len(items) - 1][4]
            db.close()
        except:
            logger.error("failed...")
            print(sys.exc_info())
            time.sleep(0.5)
            self.update_hash()

    def get_items_from_library(self):
        try:
            logger.info("getting items from library..")
            if self.cache is not None and self.cache_up_to_date:
                print("returning cache")
                return self.sort(self.cache)
            db = shelve.open(self.database_file_name)
            #items = self.sort(db.items())
            #values = [item[0] for item in items]
            values = self.sort(list(db.values()))
            db.close()
            self.cache = values
            self.cache_up_to_date = True
            return values
        except:
            print(sys.exc_info())
            logger.error("failed...")
            time.sleep(0.5)
            self.get_items_from_library()
            

    def get_item_by_hash(self, _hash):
        try:
            logger.info("getting item from library by hash..")
            db = shelve.open(self.database_file_name)
            data = None
            if self.keys_cache is not None and self.key_cache_up_to_date:
                keys = self.keys_cache
            else:
                keys = db.keys()
                self.keys_cache = keys
                self.key_cache_up_to_date = True
            if str(_hash) in keys:
                data = (db[str(_hash)])
            db.close()
            return data
        except:
            print(sys.exc_info()[0])
            logger.error("failed...")
            time.sleep(0.5)
            self.get_item_by_hash()

    def get_items_by_list_of_hashes(self, hashes):
        try:
            logger.info("getting items from library by list of hashes...")
            db = shelve.open(self.database_file_name)
            if self.keys_cache is not None and self.key_cache_up_to_date:
                keys = self.keys_cache
            else:
                keys = db.keys()
                self.keys_cache = keys
                self.key_cache_up_to_date = True
            data = []
            for hash_ in hashes:
                if str(hash_) in keys:
                    data.append((db[str(hash_)]))
            db.close()
            return self.sort(data)
        except:
            print(sys.exc_info()[0])
            logger.error("failed...")
            time.sleep(0.5)
            self.get_items_by_list_of_hashes(hashes)



    def put_item_into_library(self, value):
        try:
            logger.info("putting items into library...")
            db = shelve.open(self.database_file_name)
            mn = self.core.mediainformation_manager
            #  0 path - 1 title -2 artist - 3 album - 4 hash
            data = (value, mn.get_title(value), mn.get_artist(value), mn.get_album(value), self.hash_)
            db[str(self.hash_)] = data
            self.hash_ = self.hash_ + 1
            db.close()
            self.cache_up_to_date = False
            self.key_cache_up_to_date = False
            self.update_cache_in_background()
            return self.hash_ - 1
        except:
            print(sys.exc_info()[0])
            logger.error("failed...")
            time.sleep(0.5)
            self.put_item_into_library(value)
        
    def put_items_into_library(self, values):
        try:
            logger.info("putting mutiple items into library...")
            db = shelve.open(self.database_file_name)
            mn = self.core.mediainformation_manager
            hash_to_return = []
            for value in values:
                data = (value, mn.get_title(value), mn.get_artist(value), mn.get_album(value), self.hash_)
                db[str(self.hash_)] = data
                hash_to_return.append(self.hash_)
                self.hash_ = self.hash_ + 1
            db.close()
            self.cache_up_to_date = False
            self.key_cache_up_to_date = False
            self.update_cache_in_background()
            return hash_to_return
        except:
            print(sys.exc_info()[0])
            logger.error("failed...")
            time.sleep(0.5)
            self.put_items_into_library(values)

    def empty(self):
        try:
            logger.info("emptying db...")
            db = shelve.open(self.database_file_name)
            db.clear()
            db.close()
            self.hash_ = 0
            self.cache_up_to_date = False
            self.key_cache_up_to_date = False
            self.update_cache_in_background()
        except:
            print(sys.exc_info()[0])
            logger.error("failed...")
            time.sleep(0.5)
            self.empty()
        

    def delete_item_by_hash(self, hash_to_delete):
        try:
            logger.info("deleteing item from library by hash...")
            db = shelve.open(self.database_file_name)
            if(str(hash_to_delete) in db.keys()):
                del db[str(hash_to_delete)]
            db.close()
            self.cache_up_to_date = False
            self.key_cache_up_to_date = False
            self.update_cache_in_background()
        except:
            print(sys.exc_info()[0])
            logger.error("failed...")
            time.sleep(0.5)
            self.delete_item_by_hash(hash_to_delete)
        

    def delete_items_by_list_of_hashes(self, hashes):
        try:
            logger.info("deleteing items from library by list of hashes...")
            db = shelve.open(self.database_file_name)
            if self.keys_cache is not None and self.cache_up_to_date:
                keys = self.keys_cache
            else:
                keys = db.keys()
                self.keys_cache = keys
                self.key_cache_up_to_date = True
            data = []
            for hash_ in hashes:
                if str(hash_) in keys:
                    del db[str(hash_)]
            db.close()
            self.cache_up_to_date = False
            self.key_cache_up_to_date = False
            self.update_cache_in_background()
        except:
            print(sys.exc_info()[0])
            logger.error("failed...")
            time.sleep(0.5)
            self.delete_items_by_list_of_hashes(hashes)

    def update_cache(self):
        try:
            self.cache_is_updating = True
            logger.info("updating library cache...")
            if not self.cache_up_to_date or not self.key_cache_up_to_date:
                db = shelve.open(self.database_file_name)
                self.cache = self.sort(list(db.values()))
                self.keys_cache = list(db.keys())
                em = self.core.event_manager
                em.emit("library-update-cache-finished")
                db.close()
                self.cache_is_updating = False
                self.cache_up_to_date = True
                self.key_cache_up_to_date = True
        except:
            print(sys.exc_info()[0])
            logger.error("failed...")

    def update_cache_in_background(self):
        logger.info("starting cache update in background...")
        t = threading.Thread(target = self.update_cache)
        t.start()

    def get_key(self, item):
        return item[4]

    def sort(self, items):
        return sorted(items, key = self.get_key)    
