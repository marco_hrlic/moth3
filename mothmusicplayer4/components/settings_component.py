#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import gi
from gi.repository import Gtk

class settings_component():

    def __init__(self, manager):
        self.manager = manager
        self.get_gui_element()

    def show_settings_window(self):
        # reinitialize the window
        self.__init__(self.manager)
        self.settings_window.show()
        self.is_shown = True

    def delete_event(self, widget, event, data=None):
        return False

    def destroy(self, widget, data=None):
        self.settings_window.destroy()
        self.is_shown = False

    def get_gui_element(self):
        self.settings_window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        self.settings_window.set_title("Settings")
        self.settings_window.set_default_size(512, 512)
        self.settings_window.connect("destroy", self.destroy)

        self.is_shown = False
        scroll_window = Gtk.ScrolledWindow()
        scroll_window.grab_focus()
        scroll_window.set_shadow_type(Gtk.ShadowType.ETCHED_IN)
        scroll_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

        self.store = self.create_store()
        self.treeView = Gtk.TreeView(self.store)
        self.treeView.set_rules_hint(True)
        self.treeView.set_enable_search(True)


        self.create_columns(self.treeView)

        scroll_window.add(self.treeView)
        self.settings_window.add(scroll_window)

        scroll_window.show()
        self.treeView.show()


        #

    # /NEEDS TO BE CHANGED!!!!!!
    #
    def create_store(self):
        store = Gtk.TreeStore(str, str)
        cm = self.manager.core.configuration_manager
        data_sections = cm.get_sections()
        for section in data_sections:
            piter = store.append(None, [section, ""])
            for option in cm.get_options(section):
                store.append(piter, [option, str(cm.get_conf(section, option, "string"))])
        return store

    def create_columns(self, treeView):
        rendererText = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Property", rendererText, text=0)
        column.set_sort_column_id(0)
        treeView.append_column(column)

        rendererText = Gtk.CellRendererText()
        rendererText.set_property('editable', True)
        rendererText.connect("edited", self.cell_toggled)
        column = Gtk.TreeViewColumn("Value", rendererText, text=1)
        column.set_sort_column_id(1)
        treeView.append_column(column)

    def cell_toggled(self, cell, path, text):
        iterator = self.store.get_iter(path)
        section = self.store.get(self.store.iter_parent(iterator), 0)[0]
        option = self.store.get(iterator, 0)[0]
        self.store.set_value(iterator, 1, text)
        core = self.manager.core
        core.configuration_manager.set_conf(section, option, text)
        """if section == "keybindings":
            self.binder.get_keybindings()
            self.binder.bind_keys()"""
        if option == "show_places":
           core.filebrowser_component.places_show_hide()
        """if section == "eq":
            self.player.eq_set(int(option[4]), int(text))
        if option == "show_console":
            self.parent.parent.console.show_hide()"""
        if option == "show_infobar":
            core.gui_manager.infobar_component.show_hide()

        


