#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import os
import imp
import sys
import logging
from mothmusicplayer4 import singleton
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

class plugin_manager(metaclass=singleton.singleton):

    def __init__(self, core):
        self.core = core
        self.plugin_directory = "plugins"
        self.plugin_names = []
        self.plugins = {}

    def run(self):
        logger.info("running...")
        self.scan()
        self.load()
        self.run_plugins()

    def scan(self):
        logger.info("scanning " + self.plugin_directory + " for plugins...")
        for file_ in os.listdir(self.plugin_directory):
            if file_.endswith(".py") or file_.endswith(".pyc"):
                self.plugin_names.append(file_)

    def load(self):
        logger.info("loading up plugins...")
        for plugin_name in self.plugin_names:
            plugin = self.load_class_from_file(plugin_name)
            self.plugins[plugin_name] = plugin

    def run_plugins(self):
        logger.info("running plugins...")
        for name in self.plugin_names:
            plugin = self.plugins[name]
            if plugin is not None:
                plugin.init(self.core)
                plugin.run()


    def load_class_from_file(self, filepath):
        logger.info("loading plugin class for " + filepath + "...")
        class_inst = None
        mod_name,file_ext = os.path.splitext(os.path.split(filepath)[-1])
        if file_ext.lower() == '.py':
            py_mod = imp.load_source(mod_name, self.plugin_directory + "/" + filepath)

        elif file_ext.lower() == '.pyc':
            py_mod = imp.load_compiled(mod_name, self.plugin_directory + "/" + filepath)

        if hasattr(py_mod, filepath.split(".")[0]):
            class_inst = getattr(py_mod, filepath.split(".")[0])()

        return class_inst
