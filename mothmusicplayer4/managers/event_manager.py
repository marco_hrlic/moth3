#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from mothmusicplayer4 import singleton
import logging
import threading
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class event_manager(metaclass=singleton.singleton):

    def __init__(self, core):
        self.core = core
        #event naming (who sends)-(about what)
        self.events = ["player-about-to-finish",\
                "player-finish",\
                "player-pause",\
                "player-next-song",\
                "player-loading-track-finished",\
                "player-start-track",\
                "player-index-changed",\
                "library-updated",\
                "library-update-cache-finished",\
                "playlist-updated",\
                "application-destroy"]
        self.event_handler_map = dict.fromkeys(self.events)
        self.event_handler_data = dict.fromkeys(self.events)

    def add_event(self, event):
        logger.info("adding " + event + "...")
        if event not in self.events:
            self.events.append(event)
            self.event_handler_map[event] = None
            self.event_handler_data[event] = None
            return True
        else:
            logger.warning(event + " already exists...")
            return False

    def remove_event(self, event):
        logger.info("removing " + event + "...")
        if event in self.events:
            self.event.remove(event)
            del self.event_handler_map[event]
            del self.event_handler_data[event]
        else:
            logger.warning(event + " does not exist...")
            return False

    def register(self, event, handler, data = None):
        #data argument can be used to provide the handler
        #with additional data, variables, enviroment, etc
        #useful in scripts
        #handler must accept data argument
        logger.info( handler.__name__ + " registered to " + event + " event.")
        if(event in self.events):
            handler_list = self.event_handler_map[event]
            if handler_list is None:
                handler_list = []
            handler_list.append(handler)
            self.event_handler_map[event] = handler_list
            if data is not None:
                self.event_handler_data[event] = data

    def emit(self, event, data = None, request_thread = False):
        if(event in self.events):
            logger.info(event + " event emited")
            handler_list = self.event_handler_map[event]
            if handler_list is not None:
                for handler in handler_list:
                    #TODO
                    if request_thread:
                        t = threading.Thread(targer = handler, args = data)
                        t.start()
                    else:
                        if self.event_handler_data[event] is not None:
                            handler(data, self.event_handler_data[event])
                        else:
                            handler(data)
        else:
            logger.warning(event + " is not a internal event")
