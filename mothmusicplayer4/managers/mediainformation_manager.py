#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from mothmusicplayer4 import singleton
from mutagenx.flac import FLAC
from mutagenx.mp3 import EasyMP3 as MP3
import os

class mediainformation_manager(metaclass=singleton.singleton):

    def __init__(self, core):
        self.core = core
        self.file_type = ""
        self.audio = None

    def reverse(self, s):
        txt = ''
        for i in range(len(s) - 1, -1, -1):
            txt += s[i]
        return txt

       
    def get_file_type(self, path):
        string = os.path.basename(path)
        file_t = ""
        for c in self.reverse(string):
            if c == '.':
                break
            file_t = file_t + c
        file_t = self.reverse(file_t)
        self.file_type = file_t.upper()
        return file_t

    def get_information(self, path):
        return (self.track_get_title(path), self.track_get_artist(path), self.track_get_album(path))

    def get_title(self, path):
        try:
            self.get_file_type(path)
            string = "self.audio = " + self.file_type + "(\"" + path + "\")"
            exec(string)
            data = self.audio["title"]
            return data[0]
        except:
            title = os.path.basename(path).split("/")[-1]
            return title


    def get_artist(self, path):
        try:
            self.get_file_type(path)
            string = "self.audio = " + self.file_type + "(\"" + path + "\")"
            exec(string)
            data = self.audio["artist"]
            return data[0]
        except:
            return " - "

    def get_album(self, path):
        try:
            self.get_file_type(path)
            string = "self.audio = " + self.file_type + "(\"" + path + "\")"
            exec(string)
            data = self.audio["album"]
            return data[0]
        except:
            return " - "

    def get_duration(self, path):
        try:
            self.get_file_type(path)
            string = "self.audio = " + self.file_type + "(\"" + path + "\")"
            exec(string)
            return int(audio.info.length)
        except:
            return " - "

    def get_bitrate(self, path):
        try:
            self.get_file_type(path)
            string = "self.audio = " + self.file_type + "(\"" + path + "\")"
            exec(string)

            return str(self.audio.info.bitrate / 1000)

        except:
            return " - "

    def get_genre(self, path):
        try:
            self.get_file_type(path)
            string = "self.audio = " + self.file_type + "(\"" + path + "\")"
            exec(string)
            data = self.audio["genre"]
            # print data[0]
            return data[0]
        except:
            return " - "

    def get_year(self, path):
        try:
            self.get_file_type(path)
            string = "self.audio = " + self.file_type + "(\"" + path + "\")"
            exec(string)
            data = self.audio["date"]
            # print data[0]
            return data[0]
        except:
            return " - "

    def get_track(self, path):
        try:
            self.get_file_type(path)
            string = "self.audio = " + self.file_type + "(\"" + path + "\")"
            exec(string)
            data = self.audio["tracknumber"]
            # print data[0]
            return data[0]
        except:
            return " - "
