#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gst
from mothmusicplayer4.components import timeconverter_component
import logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

class seekbar_component():

    def __init__(self, manager):
        self.manager = manager
        self.is_seeking = False
        self.percent = 0.0
        self.duration = None

    def update_time_label(self, flag = False):
        if flag:
            self.time_label.set_text("0:00/0:00")
            return False
        self.duration = None
        self.current_position = None
        self.length = None
        self.percent = None
        self.ad = None
        pm = self.manager.core.player_manager
        if (not pm.is_playing()) and (not pm.is_paused()):
            return False
        if self.duration == None:
            try:
                self.length = pm.get_duration()
                self.duration = timeconverter_component.convert_time(self.length)
            except:
                self.duration = None
        if self.duration is not None:
            self.current_position = pm.get_current_position()
            if not self.is_seeking:
                self.time_label.set_text(self.current_position[4:] + "/" +  self.duration[4:])
                self.percent = (float(pm.get_current_position_unformatted()) / float(self.length)) * 100.0
                self.ad = Gtk.Adjustment.new(self.percent, 0.00, 100.0, 0.5, 0.5, 1.0)
                self.hscale.set_adjustment(self.ad)
        return True

    def seeking_value(self, range, scroll, value):
        pm = self.manager.core.player_manager
        if pm.is_playing or pm.is_paused:
            duration = pm.get_duration()
            time = timeconverter_component.convert_time(int((value/100.0) * duration))
            self.length = duration
            self.duration = timeconverter_component.convert_time(self.length)
            if int(time[6:]) > int(self.duration[6:]) or int(time[4:5]) > int(self.duration[4:5]):
                if not pm.is_paused():
                    return
            self.time_label.set_text(time[4:] + "/" + self.duration[4:])

    def seeker_event(self, widget, event):
        pm = self.manager.core.player_manager
        if pm.is_playing() or pm.is_paused():
            value = widget.get_value()
            duration = pm.get_duration()
            time = value * (duration / 100)
            pm.set_position(time)
            if self.is_seeking:
                self.hscale.disconnect(self.handler_id)
            self.is_seeking = False
    
    def set_seeking(self, widget, data = None):
        pm = self.manager.core.player_manager
        if pm.is_playing() or pm.is_paused:
            self.is_seeking = True
            ad = Gtk.Adjustment(self.percent, 0.00, 100.0, 0.5, 0.5, 1.0)
            self.hscale.set_adjustment(ad)
            self.handler_id = self.hscale.connect("change-value", self.seeking_value)


    def get_gui_element(self): 
        logger.info("getting gui element...")
        self.time_text = "0:00/0:00"
        box = Gtk.HBox(False, 5)
        spacer = Gtk.VSeparator()

        self.time_label = Gtk.Label(label=self.time_text)

        # self.update_time_label()
        self.hscale = Gtk.HScale()
        self.hscale.set_draw_value(False)
        self.hscale.set_value_pos(Gtk.PositionType.LEFT)
        #self.hscale.set_update_policy(Gtk.UPDATE_DISCONTINUOUS)
        self.hscale.connect("button-release-event", self.seeker_event)
        self.hscale.connect("button-press-event", self.set_seeking)
        self.hscale.set_can_focus(False)

        box.pack_start(self.time_label, False, False, 5)
        box.pack_start(spacer, False, False, 10)
        box.pack_start(self.hscale, True, True, 5)

        self.time_label.show()
        self.hscale.show()
        box.show()

        return box
