#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
import os
import logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

class filebrowser_component():

    def __init__(self, manager):
        self.manager = manager 
        self.configuration = self.manager.core.configuration_manager
        self.selection = [[''], ['']]
        self.count = 0
        self.folder = None
        self.init_filters()

    def init_filters(self):
        self.file_filter = Gtk.FileFilter()
        self.file_filter.set_name("Audio")
        self.file_filter.add_mime_type("audio/mpeg")
        self.file_filter.add_mime_type("audio/ogg")
        self.file_filter.add_pattern("*.mp3")
        self.file_filter.add_pattern("*.flac")
        self.all_filter = Gtk.FileFilter()
        self.all_filter.set_name("Everything")
        self.all_filter.add_pattern("*")

    def press_event(self, widget, event, flag = False):
        lb = self.manager.core.library_manager
        if not flag:
            keyname = Gdk.keyval_name(event.keyval)
        else:
            keyname = ""
        print(keyname)
        if keyname ==  "Shift_L" or flag:
            logger.info("inserting from file browser...")
            if flag:
                items = self.selection[(self.count) % 2]
            else:
                items = widget.get_filenames()
            file_items = []
            for item in items:
                if os.path.isfile(item):
                    file_items.append(item)
                if os.path.isdir(item):
                    items_in_dir = os.listdir(item)
                    for item_ in items_in_dir:
                        ending = item_.split(".")[-1]
                        item_ = item + "/" + item_
                        if not (ending == "mp3" or ending == "flac"):
                            continue
                        if os.path.isfile(item_):
                            file_items.append(item_)
            hash_ = lb.put_items_into_library(file_items)
            self.manager.core.event_manager.emit("library-updated", data = hash_)
                

    def selection_changed(self, a, widget):
        items = widget.get_filenames()
        self.selection[self.count] = items
        self.count = (self.count + 1) % 2

    def places_show_hide(self):
        file_box = self.file_.get_children()[0].get_children()[1].get_children()[0]
        cm = self.manager.core.configuration_manager
        if cm.get_conf("file_chooser", "show_places"):
            file_box.show()
        else:
            file_box.hide()

    def grab_focus(self):
        self.file_.grab_focus()

    def show_hide(self, state = None):
        if state is None:
            cm = self.manager.core.configuration_manager
            state = cm.get_conf("player", "show_explorer", "bool")
        if state:
            if self.folder is not None:
                self.file_.set_current_folder(self.folder)
            self.box.show()
            self.grab_focus()
        else:
            self.folder = self.file_.get_current_folder()
            self.box.hide()
            self.manager.playlist_component.grab_focus()

    def get_gui_element(self):
        logger.info("getting component gui...")
        box = Gtk.HBox(False, 0)
        file_ = Gtk.FileChooserWidget()

        file_.add_filter(self.file_filter)
        file_.add_filter(self.all_filter)
        file_.set_filter(self.file_filter)

        file_.set_action(Gtk.FileChooserAction.OPEN)
        file_.set_current_folder(self.configuration.get_conf("file_chooser", "default_load_path", "string"))
        file_.set_show_hidden(False)
        file_.set_select_multiple(True)
        file_.set_property("has-focus", False)

        #used for the enter key hacks
        file_tree = \
        file_.get_children()[0].get_children()[1].get_children()[1].get_children()[0].get_children()[0].get_children()[
            0]
        file_tree_selection = file_tree.get_selection()

        file_tree_selection.connect("changed", self.selection_changed, file_)
        file_.connect("key-press-event", self.press_event, False)
        file_.connect("file-activated", self.press_event, None, True)
        #file_.connect("current-folder-changed", self.current_folder_changed)
        
        #self.file_tree = file_tree
        self.file_ = file_
        self.places_show_hide()

        box.pack_start(file_, True, True, 0)
        self.box = box
        file_.show()
        box.show()
        return box
