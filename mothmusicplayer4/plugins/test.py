#!usr/bin/env python

import logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

class test:
    def __init__(self):
        self.core = None

    def init(self, core):
        logger.info("initializing " + __name__ + " plugin...")
        self.core = core

    def run(self):
        logger.info("running " + __name__ + " plugin...")
        pass
