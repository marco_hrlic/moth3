#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from mothmusicplayer4 import singleton
import configparser
import os

class configuration_manager(metaclass=singleton.singleton):

    def __init__(self, core):
        self.core = core
        pass

    def file_path(self):
        return os.path.split(os.path.abspath(__file__))[0] + '/data/config.cfg'

    def get_sections(self):
        config = configparser.RawConfigParser()
        with open(self.file_path(), 'r') as configfile:
            config.readfp(configfile)

        return config.sections()

    def get_options(self, section):
        config = configparser.RawConfigParser()
        with open(self.file_path(), 'r') as configfile:
            config.readfp(configfile)
        return config.options(section)

    def get_conf(self, section, option, mode = "bool"):
        config = configparser.RawConfigParser()
        with open(self.file_path(), 'r') as configfile:
            config.readfp(configfile)
        if mode is "bool":
            conf = config.getboolean(section, option)
        elif mode is "int":
            conf = config.getint(section, option)
        elif mode is "string":
            conf = config.get(section, option)
        return conf

    def set_conf(self, section, option, value):
        config = configparser.RawConfigParser()
        config.read(self.file_path())
        config.set(section, option, value)
        with open(self.file_path(), 'w') as configfile:
            config.write(configfile)
            configfile.close()



