#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class infobar_component():

    def __init__(self, manager):
        self.manager = manager
        em = self.manager.core.event_manager
        em.register("player-index-changed", self.update)
        em.register("player-start-track", self.update)

    def update(self, index):
        data = self.manager.core.gui_manager.playlist_component.store[index]
        artist = data[2]
        title = data[1]
        album = data[3]
        self.label.set_text(title + "  -  " + artist + "  -  " + album)

    def show_hide(self):
        state = self.manager.core.configuration_manager.get_conf("player", "show_infobar", "bool")
        if state:
            self.box.show()
        else: self.box.hide()

    def get_gui_element(self):
        self.box = Gtk.HBox(True)
        self.label = Gtk.Label()
        self.box.pack_start(self.label, False, False, 0)
        self.show_hide()
        self.label.show()
        return self.box

  
