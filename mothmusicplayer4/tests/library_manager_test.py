#!usr/bin/env python

from mothmusicplayer4.core import core_interface
import unittest

class test_library_manager(unittest.TestCase):
    def setUp(self):
        core = core_interface.core()
        self.lm = core.library_manager
    def test_put_get(self):
        song = "/home/marco/Music/Darkwood Dub diskografija/1997 - Darkwood Dub/Darkwood Dub - Bumerang.Chiq Toxiq Mix.mp3"
        h = self.lm.put_item_into_library(song)
        print("hash=" + str(h))
        song_ = self.lm.get_item_by_hash(h)
        self.assertEqual(song_[0], song)
    
    def test_delete(self):
        song = "/home/marco/Music/Darkwood Dub diskografija/1997 - Darkwood Dub/Darkwood Dub - Bumerang.Chiq Toxiq Mix.mp3"
        h = self.lm.put_item_into_library(song)
        self.lm.delete_item_by_hash(h)
        items = [item[0] for item in self.lm.get_items_from_library()]
        self.assertTrue(song not in items)


if __name__ == "__main__":
    unittest.main()

