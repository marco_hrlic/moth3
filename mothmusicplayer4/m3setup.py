#!usr/bin/env/python

#Core, managers, components
c = self.core
gm = c.gui_manager
em = c.event_manager
lm = c.library_manager
pc = gm.playlist_component
pcc = gm.playercontrol_component
clc = gm.commandline_component


#On startup..if there is anything in the playlist select the first row
if len(pc.store) >= 1:
    pc.selection_change(0)
    pc.grab_focus()


#Play the track as soon as you load it
def play_on_load(index, data):
    pc = data["pc"]
    pcc = data["pcc"]
    pc.selection_change(index)
    pcc.play_button_handler(True)
em.register("playlist-updated", play_on_load, {"pc": pc, "pcc": pcc})


#On application destroy empty playlist(library)
def empty_playlist(data, data2):
    lm = data2["lm"]
    lm.empty()
#em.register("application-destroy", empty_playlist, {"lm": lm})

"""
#Add play last track command
def play_last(tokens, data):
    pc = data["pc"]
    pcc = data["pcc"]
    pc.selection_change(len(pc.store) - 1)
    pcc.play_button_handler()
clc.add_command("play_last", play_last, {"pc": pc, "pcc": pcc})
clc.add_command_alias("play_last", "pl")
"""
