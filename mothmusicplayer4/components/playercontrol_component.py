#!usr/bin/env python

"""
Moth Music Player
Copyright (C) 2014  Marco Hrlic

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject, GdkPixbuf
from mothmusicplayer4.managers import configuration_manager as configuration
from mothmusicplayer4.components import seekbar_component
from mothmusicplayer4.components import settings_component
import logging
logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

class playercontrol_component():

    def __init__(self, manager):
        self.manager = manager
        self.seekbar_component = seekbar_component.seekbar_component(manager)
        self.settings_component = settings_component.settings_component(manager)

    def play_button_handler(self, data = None):
        logging.info("handlig play button...")
        pm = self.manager.core.player_manager
        pc = self.manager.playlist_component
        if not pm.is_playing() or data:
            selection = pc.get_selection()
            rows = selection.get_selected_rows()[1]
            treeiter = pc.get_iter(rows[0])
            path = pc.get_path(treeiter)
            pc.row_activated(pc.treeView, path, None, pc.store)
            
    def repeat_button_handler(self, button):
        logger.info("handling repeat button...")
        cm = self.manager.core.configuration_manager
        state = button.get_active()
        cm.set_conf("player", "repeat", str(not bool(state)))
 
    def connect_button_handlers(self):
        logger.info("connecting button handlers...")
        pm = self.manager.core.player_manager
        self.play_button.connect("pressed", self.play_button_handler)
        self.pause_button.connect("pressed", pm.pause_track)
        self.prev_button.connect("pressed", pm.start_previous_track_from_playlist)
        self.next_button.connect("pressed",pm.start_next_track_from_playlist)
        self.pref_button.connect("pressed", self.open_settings)
        self.repeat_button.connect("pressed", self.repeat_button_handler)

    def open_settings(self, data = None):
        if not self.settings_component.is_shown:
            self.settings_component.show_settings_window()
    def get_gui_element(self):
        logger.info("getting gui element...")
        cm = self.manager.core.configuration_manager
        box = Gtk.HBox(False, 5)
        box.set_border_width(5)

        spacer1 = Gtk.VSeparator()
        spacer2 = Gtk.VSeparator()

        #declare images
        image_play = Gtk.Image()
        image_pause = Gtk.Image()
        image_prev = Gtk.Image()
        image_next = Gtk.Image()
        image_repeat = Gtk.Image()
        image_pref = Gtk.Image()
        #self.image_bars = Gtk.Image()

        #set images
        #if(bool(configuration.get_conf("apperance", "use_system_gtk_theme"))):
        image_play.set_from_stock(Gtk.STOCK_MEDIA_PLAY, Gtk.IconSize.BUTTON)
        image_pause.set_from_stock(Gtk.STOCK_MEDIA_PAUSE, Gtk.IconSize.BUTTON)
        image_prev.set_from_stock(Gtk.STOCK_MEDIA_PREVIOUS, Gtk.IconSize.BUTTON)
        image_next.set_from_stock(Gtk.STOCK_MEDIA_NEXT, Gtk.IconSize.BUTTON)
        image_repeat.set_from_stock(Gtk.STOCK_REFRESH, Gtk.IconSize.BUTTON)
        image_pref.set_from_stock(Gtk.STOCK_PREFERENCES, Gtk.IconSize.BUTTON)
        """else:
            image_play.set_from_file("mmp_icons/media-playback-start.svg")
            image_pause.set_from_file("mmp_icons/media-playback-pause.svg")
            image_prev.set_from_file("mmp_icons/media-skip-backward.svg")
            image_next.set_from_file("mmp_icons/media-skip-forward.svg")
            image_repeat.set_from_file("mmp_icons/view-refresh.svg")
            image_pref.set_from_stock(Gtk.STOCK_PREFERENCES, Gtk.IconSize.BUTTON)

        self.bar_animation = GdkPixbuf.PixbufAnimation.new_from_file("mmp_icons/bar1.gif") 
        self.image_bars.set_from_pixbuf(self.bar_animation.get_static_image())
        """
        #declare buttons
        self.play_button = Gtk.Button()
        self.play_button.set_image(image_play)
        self.play_button.props.relief = Gtk.ReliefStyle.NONE
        self.play_button.set_focus_on_click(False)

        self.pause_button = Gtk.Button()
        self.pause_button.set_image(image_pause)
        self.pause_button.props.relief = Gtk.ReliefStyle.NONE
        self.pause_button.set_focus_on_click(False)

        self.prev_button = Gtk.Button()
        self.prev_button.set_image(image_prev)
        self.prev_button.props.relief = Gtk.ReliefStyle.NONE
        self.prev_button.set_focus_on_click(False)
        self.next_button = Gtk.Button()
        self.next_button.set_image(image_next)
        self.next_button.props.relief = Gtk.ReliefStyle.NONE
        self.next_button.set_focus_on_click(False)

        self.repeat_button = Gtk.ToggleButton()
        self.repeat_button.set_image(image_repeat)
        self.repeat_button.props.relief = Gtk.ReliefStyle.NONE
        self.repeat_button.set_focus_on_click(False)
        state = cm.get_conf("player", "repeat")
        if bool(state):
            self.repeat_button.set_active(True)

        self.pref_button = Gtk.Button()
        self.pref_button.set_image(image_pref)
        self.pref_button.props.relief = Gtk.ReliefStyle.NONE
        self.pref_button.set_focus_on_click(False)

        #initalize volume 
        self.volume_button = Gtk.VolumeButton()
        self.volume_button.set_value(float(cm.get_conf("player", "volume", "int")) / 100)

        #self.volume_button.connect("value-changed", self.volume_value_changed)
        #self.player.volume_track(self.volume_button.get_value() * 100)

        self.seek_bar = self.seekbar_component.get_gui_element()

        #pack the buttons
        box.pack_start(self.play_button, False, True, 0)
        box.pack_start(self.pause_button, False, True, 0)
        box.pack_start(self.prev_button, False, True, 0)
        box.pack_start(self.next_button, False, True, 0)
        box.pack_start(spacer1, False, True, 10)
        box.pack_start(self.repeat_button, False, True, 0)
        box.pack_start(spacer2, False, True, 10)
        #box.pack_start(self.image_bars, False, True, 0)
        box.pack_start(self.seek_bar, True, True, 0)
        box.pack_start(self.volume_button, False, True, 0)
        box.pack_start(self.pref_button, False, False, 10)

        #show everything
        spacer1.show()
        spacer2.show()
        self.play_button.show()
        self.pause_button.show()
        self.prev_button.show()
        self.next_button.show()
        self.repeat_button.show()
        self.pref_button.show()
        self.volume_button.show()
        #iself.image_bars.show()
        box.show()
        
        self.connect_button_handlers()

        return box

       


